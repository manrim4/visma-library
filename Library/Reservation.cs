﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library
{
    public class Reservation
    {
        public string Username { get; set; }
        public DateTime ReservationDate { get; set; }
        public TimeSpan ReservationPeriod { get; set; }

        public string ISBN { get; set; }

        public Reservation(string username, DateTime reservationDate, TimeSpan reservationPeriod, string isbn)
        {
            Username = username;
            ReservationDate = reservationDate;
            ReservationPeriod = reservationPeriod;
            ISBN = isbn;
        }

        public Reservation()
        {

        }
    }

}
