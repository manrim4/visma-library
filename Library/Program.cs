﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using Newtonsoft.Json;

namespace Library
{

    class Program
    {

        static void Main()
        {
            string booksFile = "books.json";
            string reservationsFile = "reservations.json";

            Library library = new();
            try
            {
                List<Book> books = JsonConvert.DeserializeObject<List<Book>>(File.ReadAllText(booksFile));
                List<Reservation> reservations = JsonConvert.DeserializeObject<List<Reservation>>(File.ReadAllText(reservationsFile));
                library = new(books, reservations);
            }
            catch (FileNotFoundException)
            {
            }

            List<Book> bookList = new();

            Console.WriteLine("Welcome to the library!");
            Console.WriteLine("Available commands: list, available, taken, filter, take, add, delete, return, exit");
            while (true)
            {
                Console.Write("Visma Library> ");
                string command = Console.ReadLine().Trim().ToLower();
                switch (command)
                {
                    case "":
                        break;
                    case "exit":
                        Environment.Exit(0);
                        break;
                    case "take":
                        if (TakeBook(library, bookList))
                            library.SaveToJson(booksFile, reservationsFile);
                        break;
                    case "list":
                        bookList = ListBooks(library);
                        break;
                    case "taken":
                        bookList = ListTaken(library);
                        break;
                    case "available":
                        bookList = ListAvailable(library);
                        break;
                    case "add":
                        if (AddBook(library))
                            library.SaveToJson(booksFile, reservationsFile);
                        break;
                    case "delete":
                        if (DeleteBook(library, bookList))
                            library.SaveToJson(booksFile, reservationsFile);
                        break;
                    case "return":
                        if (ReturnBook(library))
                            library.SaveToJson(booksFile, reservationsFile);
                        break;
                    case "filter":
                        FilterBooks(library);
                        break;
                    default:
                        Console.WriteLine("Unrecognized command");
                        break;
                }
            }
        }

        /// <summary>
        /// Adds a book to the library
        /// </summary>
        /// <param name="library">Library where the book is being added to</param>
        /// <returns> Book addition status </returns>
        static bool AddBook(Library library)
        {
            Book newBook = BookInfoDialogue();
            ListBook(newBook);
            Console.WriteLine("Do you want to add this book? Y/N");
            if (GetYN())
                if (library.AddBook(newBook))
                {
                    Console.WriteLine("Book added!");
                    return true;
                }
                else
                    Console.WriteLine("Error adding the book!");
            return false;
        }

        /// <summary>
        /// Takes a book from the library
        /// </summary>
        /// <param name="library">Library where the book is being taken from</param>
        /// <param name="bookList"> List of the listed books on the terminal </param>
        /// <returns> Book reservation status </returns>
        static bool TakeBook(Library library, List<Book> bookList)
        {

            if (bookList.Count == 0 || !bookList.Where(x => !x.Taken).Any())
            {
                Console.WriteLine("There are no books to take. Please list/filter them first or add one");
                return false;
            }
            Reservation newReservation = ReservationInfoDialogue(bookList);
            Console.WriteLine("Is the data correct? Y/N");
            ListReservation(newReservation);
            if (GetYN())
                if (library.ReservationsCount(newReservation.Username) > 2)
                {
                    Console.WriteLine("Sorry, you have taken too many books already!");
                }
                else
                {
                    if (library.AddReservation(newReservation))
                    {
                        library.TakeBook(newReservation.ISBN);
                        Console.WriteLine("Book taken!");
                        return true;
                    }
                    else
                        Console.WriteLine("Error taking the book!");
                }
            return false;
        }

        /// <summary>
        /// Returns book to the library
        /// </summary>
        /// <param name="library"> Library where the book is being returned </param>
        /// <returns> Book return status </returns>
        static bool ReturnBook(Library library)
        {
            Console.WriteLine("Enter your username:");
            string username = ReadCheckString();
            List<Reservation> reservations = library.FilterByUsername(username);
            if (reservations.Count == 0)
            {
                Console.WriteLine("There are no books to return.");
                return false;
            }
            ListReservations(reservations);
            Console.WriteLine("Choose the book you would like to return:");
            int id;
            while (!int.TryParse(Console.ReadLine(), out id) || id > library.Reservations.Count || id < 1)
            {
                Console.WriteLine("Invalid ID. Please try again!");
            }
            ListReservation(reservations[id - 1]);
            Console.WriteLine("Do you want to return this book? Y/N");
            if (GetYN())
            {
                if ((reservations[id - 1].ReservationDate + reservations[id - 1].ReservationPeriod) < DateTime.Now)
                    Console.WriteLine("Uh oh! Someone is late!");
                if (library.RemoveReservation(reservations[id - 1]))
                {
                    library.ReturnBook(reservations[id - 1].ISBN);
                    Console.WriteLine("Book returned!");
                    return true;
                }
                else
                    Console.WriteLine("Error returning the book!");
            }
            return false;
        }

        /// <summary>
        /// Deletes a book from the library
        /// </summary>
        /// <param name="library"> Library where the book is deleted </param>
        /// <param name="bookList"> List of the listed books on the terminal </param>
        /// <returns></returns>
        static bool DeleteBook(Library library, List<Book> bookList)
        {
            if (bookList.Count == 0)
                Console.WriteLine("There are no books to take. Please list/filter them first or add one");
            else
            {
                Console.WriteLine("Enter the ID of the book you would like to take:");
                int id;
                while (!int.TryParse(Console.ReadLine(), out id) || id > bookList.Count || id < 1)
                {
                    Console.WriteLine("Invalid ID. Please try again!");
                }
                ListBook(bookList[id - 1]);
                Console.WriteLine("Do you want to delete this book? Y/N");
                if (GetYN())
                    if (library.RemoveBook(bookList[id - 1]))
                    {
                        Console.WriteLine("Book removed!");
                        return true;
                    }
                    else
                        Console.WriteLine("Error removing the book!");
            }
            return false;
        }

        /// <summary>
        /// Lists all books in the terminal
        /// </summary>
        /// <param name="library"> Library that is listed </param>
        /// <returns> list of books </returns>
        static List<Book> ListBooks(Library library)
        {
            List<Book> bookList = library.Books;
            if (bookList.Count == 0)
                Console.WriteLine("There are no books to show. Please add more books to the library.");
            else
                ListBooks(bookList);
            return bookList;
        }

        /// <summary>
        /// Lists filtered books in the terminal
        /// </summary>
        /// <param name="library"> Library that is filtered </param>
        /// <returns> new filtered list </returns>
        static List<Book> FilterBooks(Library library)
        {
            List<Book> bookList = new();
            try
            {
                bookList = Filters(library);
                if (bookList.Count == 0)
                    Console.WriteLine("There are no books to show. Please add more books to the library or change the filters.");
                else
                    ListBooks(bookList);
            }
            catch (FieldAccessException)
            {
                Console.WriteLine("Such field does not exist!");
            }
            return bookList;
        }

        /// <summary>
        /// Lists taken books in the terminal
        /// </summary>
        /// <param name="library"> Library that is filtered </param>
        /// <returns> new filtered list </returns>
        static List<Book> ListTaken(Library library)
        {
            List<Book> bookList = library.FilterTaken();
            if (bookList.Count == 0)
                Console.WriteLine("There are no books to show.");
            else
                ListBooks(bookList);
            return bookList;
        }

        /// <summary>
        /// Lists available books in the terminal
        /// </summary>
        /// <param name="library"> Library that is filtered </param>
        /// <returns> new filtered list </returns>
        static List<Book> ListAvailable(Library library)
        {
            List<Book> bookList = library.FilterAvailable();
            if (bookList.Count == 0)
                Console.WriteLine("There are no books to show.");
            else
                ListBooks(bookList);
            return bookList;
        }

        /// <summary>
        /// Keeps asking for input in the terminal until it gets a non empty string from the user
        /// </summary>
        /// <returns> Trimmed input as a string </returns>
        static string ReadCheckString()
        {
            string value = Console.ReadLine().Trim();
            while (value == "")
            {
                Console.WriteLine("Field cannot be empty! Please input the value!");
                value = Console.ReadLine().Trim();
            }
            return value;
        }

        /// <summary>
        /// Keeps asking for input in the terminal until it gets straight Y or N from the user
        /// </summary>
        /// <returns> false for N, true for Y </returns>
        static bool GetYN()
        {
            string value = Console.ReadLine().Trim().ToLower();
            while (true)
            {
                if (value == "n")
                    return false;
                if (value == "y")
                    return true;
                Console.WriteLine("Unrecognized input. Please enter Y or N");
                value = Console.ReadLine().Trim().ToLower();
            }
        }

        /// <summary>
        /// Directs to a proper filtering method from user inputs
        /// </summary>
        /// <param name="library"> Library that is filtered </param>
        /// <returns> Filtered items </returns>
        static List<Book> Filters(Library library)
        {
            Console.WriteLine("What field would you like to filter by?");
            string field = Console.ReadLine();
            Console.WriteLine("Enter your search query:");
            string query = Console.ReadLine();
            return field switch
            {
                "author" => library.FilterByAuthor(query),
                "category" => library.FilterByCategory(query),
                "language" => library.FilterByLanguage(query),
                "isbn" => library.FilterByISBN(query),
                "name" => library.FilterByName(query),
                _ => throw new FieldAccessException(),
            };
        }

        /// <summary>
        /// Creates a new Reservation object from user inputs
        /// </summary>
        /// <param name="isbn">ISBN of a book that is about to be reserved</param>
        /// <returns>new Reservation object</returns>
        static Reservation ReservationInfoDialogue(List<Book> books)
        {
            Console.WriteLine("Enter the ID of the book you would like to take:");
            int id;
            while (!int.TryParse(Console.ReadLine(), out id) || id > books.Count || id < 1 || books[id - 1].Taken)
            {
                Console.WriteLine("Invalid ID. Please try again!");
            }
            Console.WriteLine("Enter your username:");
            string username = ReadCheckString();
            Console.WriteLine("Enter the reservation period in days:");
            int period;
            while (!int.TryParse(Console.ReadLine(), out period) || period > 60 || period < 1)
            {
                if (period > 60)
                    Console.WriteLine("Reservation period cannot be longer than 2 months.");
                else
                    Console.WriteLine("Invalid time period. Please try again!");
            }
            TimeSpan reservationPeriod = TimeSpan.FromDays(period);
            DateTime reservationDate = DateTime.Now;
            return new Reservation(username, reservationDate, reservationPeriod, books[id - 1].ISBN);
        }

        /// <summary>
        /// Creates a new Book object from user inputs
        /// </summary>
        /// <returns>new Book object</returns>
        static Book BookInfoDialogue()
        {
            Console.WriteLine("Enter book name:");
            string name = ReadCheckString();
            Console.WriteLine("Enter book author:");
            string author = ReadCheckString();
            Console.WriteLine("Enter book category:");
            string category = ReadCheckString();
            Console.WriteLine("Enter book language:");
            string language = ReadCheckString();
            Console.WriteLine("Enter book publication date:");
            DateTime publicationDate;
            while (!DateTime.TryParseExact(Console.ReadLine(), "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None, out publicationDate))
            {
                Console.WriteLine("Date is not correct! Please input date in a format YYYY-MM-DD");
            }
            Console.WriteLine("Enter book ISBN:");
            string isbn = ReadCheckString();
            return new Book(name, author, category, language, publicationDate, isbn);
        }

        /// <summary>
        /// Lists all the reservations from the list in the terminal 
        /// </summary>
        /// <param name="reservations">List of the reservations to be listed</param>
        static void ListReservations(List<Reservation> reservations)
        {
            Console.WriteLine("| {0,-5} | {1,-20} | {2,-20} | {3,-20} | {4,-10} |",
                "ID", "Username", "Reservation date", "Return by", "ISBN");
            for (int i = 0; i < reservations.Count; i++)
            {
                Console.WriteLine("| {0,-5} | {1,-20} | {2,-20} | {3,-20} | {4,-10} |",
                    i + 1, reservations[i].Username, reservations[i].ReservationDate, reservations[i].ReservationDate + reservations[i].ReservationPeriod, reservations[i].ISBN);
            }
        }

        /// <summary>
        /// Lists a single reservation in the terminal 
        /// </summary>
        /// <param name="reservation">Reservation to be listed</param>
        static void ListReservation(Reservation reservation)
        {
            Console.WriteLine("| {0,-15} | {1,-20} | {2,-20} | {3,-10} |",
                 "Username", "Reservation date", "Return by", "ISBN");

            Console.WriteLine("| {0,-15} | {1,-20} | {2,-20} | {3,-10} |",
               reservation.Username, reservation.ReservationDate, reservation.ReservationDate + reservation.ReservationPeriod, reservation.ISBN);

        }

        /// <summary>
        /// Lists all the books from the list in the terminal 
        /// </summary>
        /// <param name="books">List of the books to be listed</param>
        static void ListBooks(List<Book> books)
        {
            Console.WriteLine("| {0,-5} | {1,-15} | {2,-15} | {3,-15} | {4,-10} | {5,-10} | {6,-13} | {7,-5} |",
                "ID", "Name", "Author", "Category", "Language", "Pub. date", "ISBN", "Taken");
            for (int i = 0; i < books.Count; i++)
            {
                Console.WriteLine("| {0,-5} | {1,-15} | {2,-15} | {3,-15} | {4,-10} | {5,-10} | {6,-13} | {7,-5} |",
                    i + 1, books[i].Name, books[i].Author, books[i].Category, books[i].Language, books[i].PublicationDate.ToString("yyyy-MM-dd"), books[i].ISBN, books[i].Taken);
            }
        }

        /// <summary>
        /// Lists a single book in the terminal 
        /// </summary>
        /// <param name="book">Book to be listed</param>
        static void ListBook(Book book)
        {
            Console.WriteLine("| {0,-15} | {1,-15} | {2,-15} | {3,-10} | {4,-10} | {5,-13} | {6,-5} |",
                 "Name", "Author", "Category", "Language", "Pub. date", "ISBN", "Taken");
            Console.WriteLine("| {0,-15} | {1,-15} | {2,-15} | {3,-10} | {4,-10} | {5,-13} | {6,-5} |",
                 book.Name, book.Author, book.Category, book.Language, book.PublicationDate.ToString("yyyy-MM-dd"), book.ISBN, book.Taken);
        }
    }
}
