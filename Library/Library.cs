﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library
{
    public class Library
    {
        public List<Book> Books { get; }
        public List<Reservation> Reservations { get; }

        public Library()
        {
            Books = new();
            Reservations = new();
        }

        public Library(List<Book> books, List<Reservation> reservations)
        {
            Books = books;
            Reservations = reservations;
        }

        public bool TakeBook(string ISBN)
        {
            try
            {
                Books.Find(x => x.ISBN == ISBN && x.Taken == false).Taken = true;
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool ReturnBook(string ISBN)
        {
            try
            {
                Books.Find(x => x.ISBN == ISBN && x.Taken == true).Taken = false;
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool AddBook(Book book)
        {
            try
            {
                Books.Add(book);
                return true;
            }
            catch (Exception) { return false; }

        }

        public bool AddReservation(Reservation reservation)
        {
            try
            {
                Reservations.Add(reservation);
                return true;
            }
            catch (Exception) { return false; }

        }

        public bool RemoveReservation(Reservation reservation)
        {
            try
            {
                Reservations.Remove(reservation);
                return true;
            }
            catch (Exception) { return false; }

        }

        public bool RemoveBook(Book book)
        {
            try
            {
                Books.Remove(book);
                // Reservations.RemoveAll((x) => x.ISBN.Equals(book.ISBN));
                return true;
            }
            catch (Exception) { return false; }
        }

        public bool RemoveBook(string ISBN)
        {
            try
            {
                Books.RemoveAll(x => x.ISBN.Equals(ISBN));
                //Reservations.RemoveAll(x => x.ISBN.Equals(ISBN));
                return true;
            }
            catch (Exception) { return false; }
        }

        public int ReservationsCount(string username)
        {
            int count = 0;
            foreach (Reservation reservation in Reservations)
            {
                if (username.Equals(reservation.Username))
                    count++;
            }
            return count;
        }
        public List<Reservation> FilterByUsername(string filter)
        {
            List<Reservation> filtered = new();
            foreach (Reservation reservation in Reservations)
                if (reservation.Username.Equals(filter))
                    filtered.Add(reservation);
            return filtered;
        }

        public List<Book> FilterByAuthor(string filter)
        {
            List<Book> filtered = new();
            foreach (Book book in Books)
                if (book.Author.Contains(filter))
                    filtered.Add(book);
            return filtered;
        }

        public List<Book> FilterByCategory(string filter)
        {
            List<Book> filtered = new();
            foreach (Book book in Books)
                if (book.Category.Contains(filter))
                    filtered.Add(book);
            return filtered;
        }
        public List<Book> FilterByLanguage(string filter)
        {
            List<Book> filtered = new();
            foreach (Book book in Books)
                if (book.Language.Contains(filter))
                    filtered.Add(book);
            return filtered;
        }
        public List<Book> FilterByISBN(string filter)
        {
            List<Book> filtered = new();
            foreach (Book book in Books)
                if (book.ISBN.Contains(filter))
                    filtered.Add(book);
            return filtered;
        }
        public List<Book> FilterByName(string filter)
        {
            List<Book> filtered = new();
            foreach (Book book in Books)
                if (book.Name.Contains(filter))
                    filtered.Add(book);
            return filtered;
        }
        public List<Book> FilterTaken()
        {
            List<Book> filtered = new();
            foreach (Book book in Books)
                if (book.Taken)
                    filtered.Add(book);
            return filtered;
        }
        public List<Book> FilterAvailable()
        {
            List<Book> filtered = new();
            foreach (Book book in Books)
                if (!book.Taken)
                    filtered.Add(book);
            return filtered;
        }

        public bool SaveToJson(string booksFile, string reservationsFile)
        {
            try
            {
                string jsonBooks = JsonConvert.SerializeObject(Books);
                File.WriteAllText(booksFile, jsonBooks);
                string jsonReservations = JsonConvert.SerializeObject(Reservations);
                File.WriteAllText(reservationsFile, jsonReservations);
                return true;
            }
            catch (Exception) { return false; }
        }
    }
}
