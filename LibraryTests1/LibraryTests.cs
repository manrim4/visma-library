﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Library;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Tests
{
    [TestClass()]
    public class LibraryTests
    {
        [TestMethod()]
        public void LibraryTest()
        {
            Library library = new();
            List<Reservation> reservations = new();
            List<Book> books = new();
            Library library2 = new(books, reservations);

            Assert.AreEqual(library2.Books.Count, library.Books.Count);
        }

        [TestMethod()]
        public void AddBookTest()
        {
            Library library = new();
            Book book = new();
            var res = library.AddBook(book);
            if (!res)
                Assert.Fail();
            Assert.AreEqual(1, library.Books.Count);
        }

        [TestMethod()]
        public void AddReservationTest()
        {
            Library library = new();
            Reservation reservation = new();
            var res = library.AddReservation(reservation);
            if (!res)
                Assert.Fail();

            Assert.AreEqual(1, library.Reservations.Count);
        }

        [TestMethod()]
        public void RemoveReservationTest()
        {
            List<Reservation> reservations = new();
            reservations.Add(new Reservation("uname", DateTime.Now, TimeSpan.FromDays(15), "123456789"));
            reservations.Add(new Reservation("uname2", DateTime.Now, TimeSpan.FromDays(11), "1234567"));
            reservations.Add(new Reservation("uname3", DateTime.Now, TimeSpan.FromDays(18), "12345678"));
            List<Book> books = new();

            Library library = new(books, reservations);
            var res = library.RemoveReservation(reservations[1]);
            if (!res)
                Assert.Fail();

            Assert.AreEqual(2, library.Reservations.Count);
        }

        [TestMethod()]
        public void RemoveBookTest()
        {
            List<Reservation> reservations = new();
            List<Book> books = new();
            books.Add(new Book("title", "author", "cat", "lang", DateTime.Now, "123456"));
            books.Add(new Book("title2", "author", "cat", "lang", DateTime.Now, "123"));
            books.Add(new Book("title", "author2", "cat", "lang", DateTime.Now, "456"));
            Library library = new(books, reservations);
            var res = library.RemoveBook(books[1]);
            if (!res)
                Assert.Fail();

            Assert.AreEqual(2, library.Books.Count);
        }

        [TestMethod()]
        public void RemoveBookTest1()
        {
            List<Reservation> reservations = new();
            List<Book> books = new();
            books.Add(new Book("title", "author", "cat", "lang", DateTime.Now, "123456"));
            books.Add(new Book("title2", "author", "cat", "lang", DateTime.Now, "123"));
            books.Add(new Book("title", "author2", "cat", "lang", DateTime.Now, "456"));
            Library library = new(books, reservations);
            var res = library.RemoveBook("123");
            if (!res)
                Assert.Fail();

            Assert.AreEqual(2, library.Books.Count);
        }

        [TestMethod()]
        public void ReservationsCountTest()
        {
            List<Reservation> reservations = new();
            reservations.Add(new Reservation("uname", DateTime.Now, TimeSpan.FromDays(15), "123456789"));
            reservations.Add(new Reservation("uname2", DateTime.Now, TimeSpan.FromDays(11), "1234567"));
            reservations.Add(new Reservation("uname", DateTime.Now, TimeSpan.FromDays(18), "12345678"));
            List<Book> books = new();

            Library library = new(books, reservations);
            var res = library.ReservationsCount("uname");

            Assert.AreEqual(2, res);
        }

        [TestMethod()]
        public void FilterByUsernameTest()
        {
            List<Reservation> reservations = new();
            reservations.Add(new Reservation("uname", DateTime.Now, TimeSpan.FromDays(15), "123456789"));
            reservations.Add(new Reservation("uname2", DateTime.Now, TimeSpan.FromDays(11), "1234567"));
            reservations.Add(new Reservation("uname", DateTime.Now, TimeSpan.FromDays(18), "12345678"));
            List<Book> books = new();

            Library library = new(books, reservations);
            var res = library.FilterByUsername("uname");

            Assert.AreEqual(2, res.Count);
        }

        [TestMethod()]
        public void FilterByAuthorTest()
        {
            List<Reservation> reservations = new();
            List<Book> books = new();
            books.Add(new Book("title", "author", "cat", "lang", DateTime.Now, "123456"));
            books.Add(new Book("title2", "author", "cat", "lang", DateTime.Now, "123"));
            books.Add(new Book("title", "author2", "cat", "lang", DateTime.Now, "456"));
            Library library = new(books, reservations);
            var res = library.FilterByAuthor("author");

            Assert.AreEqual(3, res.Count);
        }

        [TestMethod()]
        public void FilterByAuthorTest1()
        {
            List<Reservation> reservations = new();
            List<Book> books = new();
            books.Add(new Book("title", "author", "cat", "lang", DateTime.Now, "123456"));
            books.Add(new Book("title2", "author", "cat", "lang", DateTime.Now, "123"));
            books.Add(new Book("title", "author2", "cat", "lang", DateTime.Now, "456"));
            Library library = new(books, reservations);
            var res = library.FilterByAuthor("r2");

            Assert.AreEqual(1, res.Count);
        }

        [TestMethod()]
        public void FilterByCategoryTest()
        {
            List<Reservation> reservations = new();
            List<Book> books = new();
            books.Add(new Book("title", "author", "cat", "lang", DateTime.Now, "123456"));
            books.Add(new Book("title2", "author", "caat", "lang", DateTime.Now, "123"));
            books.Add(new Book("title", "author2", "catt", "lang", DateTime.Now, "456"));
            Library library = new(books, reservations);
            var res = library.FilterByCategory("cat");

            Assert.AreEqual(2, res.Count);
        }

        [TestMethod()]
        public void FilterByLanguageTest()
        {
            List<Reservation> reservations = new();
            List<Book> books = new();
            books.Add(new Book("title", "author", "cat", "lang", DateTime.Now, "123456"));
            books.Add(new Book("title2", "author", "caat", "laang", DateTime.Now, "123"));
            books.Add(new Book("title", "author2", "catt", "llangg", DateTime.Now, "456"));
            Library library = new(books, reservations);
            var res = library.FilterByLanguage("lang");

            Assert.AreEqual(2, res.Count);
        }

        [TestMethod()]
        public void FilterByISBNTest()
        {
            List<Reservation> reservations = new();
            List<Book> books = new();
            books.Add(new Book("title", "author", "cat", "lang", DateTime.Now, "123456"));
            books.Add(new Book("title2", "author", "caat", "lang", DateTime.Now, "123"));
            books.Add(new Book("title", "author2", "catt", "lang", DateTime.Now, "456"));
            Library library = new(books, reservations);
            var res = library.FilterByISBN("23");

            Assert.AreEqual(2, res.Count);
        }

        [TestMethod()]
        public void FilterByNameTest()
        {
            List<Reservation> reservations = new();
            List<Book> books = new();
            books.Add(new Book("title", "author", "cat", "lang", DateTime.Now, "123456"));
            books.Add(new Book("title2", "author", "caat", "lang", DateTime.Now, "123"));
            books.Add(new Book("ttttitle2", "author2", "catt", "lang", DateTime.Now, "456"));
            Library library = new(books, reservations);
            var res = library.FilterByName("title2");

            Assert.AreEqual(2, res.Count);
        }

        [TestMethod()]
        public void FilterTakenTest()
        {
            List<Reservation> reservations = new();
            List<Book> books = new();
            books.Add(new Book("title", "author", "cat", "lang", DateTime.Now, "123456"));
            books.Add(new Book("title2", "author", "caat", "lang", DateTime.Now, "123"));
            books.Add(new Book("title", "author2", "catt", "lang", DateTime.Now, "456"));
            Library library = new(books, reservations);
            library.TakeBook("123");
            library.TakeBook("123");
            var res = library.FilterTaken();

            Assert.AreEqual(1, res.Count);
        }

        [TestMethod()]
        public void FilterAvailableTest()
        {
            List<Reservation> reservations = new();
            List<Book> books = new();
            books.Add(new Book("title", "author", "cat", "lang", DateTime.Now, "123456"));
            books.Add(new Book("title2", "author", "caat", "lang", DateTime.Now, "123"));
            books.Add(new Book("title", "author2", "catt", "lang", DateTime.Now, "456"));
            Library library = new(books, reservations);
            library.TakeBook("123");
            library.TakeBook("123");
            var res = library.FilterTaken();

            Assert.AreEqual(1, res.Count);
        }


        [TestMethod()]
        public void TakeBookTest()
        {
            List<Reservation> reservations = new();
            List<Book> books = new();
            books.Add(new Book("title", "author", "cat", "lang", DateTime.Now, "123456"));
            books.Add(new Book("title2", "author", "caat", "lang", DateTime.Now, "123"));
            books.Add(new Book("title", "author2", "catt", "lang", DateTime.Now, "456"));
            Library library = new(books, reservations);
            library.TakeBook("123");

            Assert.AreEqual(true, books[1].Taken);
        }

        [TestMethod()]
        public void ReturnBookTest()
        {
            List<Reservation> reservations = new();
            List<Book> books = new();
            books.Add(new Book("title", "author", "cat", "lang", DateTime.Now, "123456"));
            books.Add(new Book("title2", "author", "caat", "lang", DateTime.Now, "123"));
            books.Add(new Book("title", "author2", "catt", "lang", DateTime.Now, "123", true));
            Library library = new(books, reservations);
            library.ReturnBook("123");

            Assert.AreEqual(false, books[2].Taken);
        }

    }
}